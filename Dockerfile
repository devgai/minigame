FROM sergeyzh/centos6-java:jdk8
ENV TZ=Asia/Ho_Chi_Minh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone 
# Adding cacerts file from current Java disto to "conf" directory of Tomcat
ADD . /var/www/
RUN chmod -R 777 /var/www/* && cd /var/www/ && sed -i -e 's/\r$//' main